package threads.lite.noise;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import threads.lite.core.Stream;
import threads.lite.core.Transport;
import threads.lite.host.LiteStream;

public class Handshake implements Transport {
    @NonNull
    private final QuicStream stream;

    public Handshake(@NonNull QuicStream stream) {
        this.stream = stream;
    }


    @Override
    public Type getType() {
        return Type.HANDSHAKE;
    }

    @Override
    public Stream getStream() {
        return new LiteStream(stream);
    }
}
