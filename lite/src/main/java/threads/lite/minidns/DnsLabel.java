/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

/**
 * A DNS label is an individual component of a DNS name. Labels are usually shown separated by dots.
 * <p>
 * This class implements {@link Comparable} which compares DNS labels according to the Canonical DNS Name Order as
 * specified in <a href="https://tools.ietf.org/html/rfc4034#section-6.1">RFC 4034 § 6.1</a>.
 * </p>
 * <p>
 * Note that as per <a href="https://tools.ietf.org/html/rfc2181#section-11">RFC 2181 § 11</a> DNS labels may contain
 * any byte.
 * </p>
 *
 * @author Florian Schmaus
 * @see <a href="https://tools.ietf.org/html/rfc5890#section-2.2">RFC 5890 § 2.2. DNS-Related Terminology</a>
 */
public abstract class DnsLabel extends SafeCharSequence implements Comparable<DnsLabel> {

    /**
     * The maximum length of a DNS label in octets.
     *
     * @see <a href="https://tools.ietf.org/html/rfc1035">RFC 1035 § 2.3.4.</a>
     */
    public static final int MAX_LABEL_LENGTH_IN_OCTETS = 63;

    /**
     * Whether or not the DNS label is validated on construction.
     */
    public static final boolean VALIDATE = true;

    public final String label;
    private transient String safeToStringRepresentation;
    private transient DnsLabel lowercasedVariant;
    private transient byte[] byteCache;

    protected DnsLabel(String label) {
        this.label = label;

        if (!VALIDATE) {
            return;
        }

        setBytesIfRequired();
        if (byteCache.length > MAX_LABEL_LENGTH_IN_OCTETS) {
            throw new LabelToLongException(label);
        }
    }

    public static DnsLabel from(String label) {
        if (label == null || label.isEmpty()) {
            throw new IllegalArgumentException("Label is null or empty");
        }

        if (LdhLabel.isLdhLabel(label)) {
            return LdhLabel.fromInternal(label);
        }

        return NonLdhLabel.fromInternal(label);
    }

    public static DnsLabel[] from(String[] labels) {
        DnsLabel[] res = new DnsLabel[labels.length];

        for (int i = 0; i < labels.length; i++) {
            res[i] = DnsLabel.from(labels[i]);
        }

        return res;
    }

    public static boolean isIdnAcePrefixed(String string) {
        return string.toLowerCase(Locale.US).startsWith("xn--");
    }

    public static String toSafeRepesentation(String dnsLabel) {
        if (consistsOnlyOfLettersDigitsHypenAndUnderscore(dnsLabel)) {
            // This label is safe, nothing to do.
            return dnsLabel;
        }

        StringBuilder sb = new StringBuilder(2 * dnsLabel.length());
        for (int i = 0; i < dnsLabel.length(); i++) {
            char c = dnsLabel.charAt(i);
            if (isLdhOrMaybeUnderscore(c, true)) {
                sb.append(c);
                continue;
            }


            // Let's see if we found and unsafe char we want to replace.
            switch (c) {
                case '.':
                    sb.append('●'); // U+25CF BLACK CIRCLE;
                    break;
                case '\\':
                    sb.append('⧷'); // U+29F7 REVERSE SOLIDUS WITH HORIZONTAL STROKE
                    break;
                case '\u007f':
                    // Convert DEL to U+2421 SYMBOL FOR DELETE
                    sb.append('␡');
                    break;
                case ' ':
                    sb.append('␣'); // U+2423 OPEN BOX
                    break;
                default:
                    if (c < 32) {
                        // First convert the ASCI control codes to the Unicode Control Pictures
                        int substituteAsInt = c + '\u2400';
                        char substitute = (char) substituteAsInt;
                        sb.append(substitute);
                    } else if (c < 127) {
                        // Everything smaller than 127 is now safe to directly append.
                        sb.append(c);
                    } else if (c > 255) {
                        throw new IllegalArgumentException("The string '" + dnsLabel
                                + "' contains characters outside the 8-bit range: " + c + " at position " + i);
                    } else {
                        // Everything that did not match the previous conditions is explicitly escaped.
                        sb.append("〚"); // U+301A
                        // Transform the char to hex notation. Note that we have ensure that c is <= 255
                        // here, hence only two hexadecimal places are ok.
                        String hex = String.format("%02X", (int) c);
                        sb.append(hex);
                        sb.append("〛"); // U+301B
                    }
            }
        }

        return sb.toString();
    }

    private static boolean isLdhOrMaybeUnderscore(char c, boolean underscore) {
        return (c >= 'a' && c <= 'z')
                || (c >= 'A' && c <= 'Z')
                || (c >= '0' && c <= '9')
                || c == '-'
                || (underscore && c == '_')
                ;
    }

    private static boolean consistsOnlyOfLdhAndMaybeUnderscore(String string, boolean underscore) {
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            if (isLdhOrMaybeUnderscore(c, underscore)) {
                continue;
            }
            return false;
        }
        return true;
    }

    public static boolean consistsOnlyOfLettersDigitsAndHypen(String string) {
        return consistsOnlyOfLdhAndMaybeUnderscore(string, false);
    }

    public static boolean consistsOnlyOfLettersDigitsHypenAndUnderscore(String string) {
        return consistsOnlyOfLdhAndMaybeUnderscore(string, true);
    }

    @NonNull
    @Override
    public final String toString() {
        if (safeToStringRepresentation == null) {
            safeToStringRepresentation = toSafeRepesentation(label);
        }

        return safeToStringRepresentation;
    }

    @Override
    public final boolean equals(Object other) {
        if (!(other instanceof DnsLabel)) {
            return false;
        }
        DnsLabel otherDnsLabel = (DnsLabel) other;
        return label.equals(otherDnsLabel.label);
    }

    @Override
    public final int hashCode() {
        return label.hashCode();
    }

    public final DnsLabel asLowercaseVariant() {
        if (lowercasedVariant == null) {
            String lowercaseLabel = label.toLowerCase(Locale.US);
            lowercasedVariant = DnsLabel.from(lowercaseLabel);
        }
        return lowercasedVariant;
    }

    private void setBytesIfRequired() {
        if (byteCache == null) {
            byteCache = label.getBytes(StandardCharsets.US_ASCII);
        }
    }

    public final void writeToBoas(ByteArrayOutputStream byteArrayOutputStream) {
        setBytesIfRequired();

        byteArrayOutputStream.write(byteCache.length);
        byteArrayOutputStream.write(byteCache, 0, byteCache.length);
    }

    @Override
    public final int compareTo(DnsLabel other) {
        String myCanonical = asLowercaseVariant().label;
        String otherCanonical = other.asLowercaseVariant().label;

        return myCanonical.compareTo(otherCanonical);
    }

    public static class LabelToLongException extends IllegalArgumentException {
        public final String label;

        LabelToLongException(String label) {
            this.label = label;
        }
    }


    public static final class ALabel extends XnLabel {

        ALabel(String label) {
            super(label);
        }

    }

    public static final class FakeALabel extends XnLabel {

        FakeALabel(String label) {
            super(label);
        }

    }


    /**
     * A DNS label with a leading or trailing hyphen ('-').
     */
    public static final class LeadingOrTrailingHyphenLabel extends NonLdhLabel {

        LeadingOrTrailingHyphenLabel(String label) {
            super(label);
        }

        static boolean isLeadingOrTrailingHypenLabelInternal(String label) {
            if (label.isEmpty()) {
                return false;
            }

            if (label.charAt(0) == '-') {
                return true;
            }

            return label.charAt(label.length() - 1) == '-';
        }
    }


    /**
     * A Non-Reserved LDH label (NR-LDH label), which do <em>not</em> have "--" in the third and fourth position.
     */
    public static final class NonReservedLdhLabel extends LdhLabel {

        NonReservedLdhLabel(String label) {
            super(label);
            assert isNonReservedLdhLabelInternal(label);
        }

        static boolean isNonReservedLdhLabelInternal(String label) {
            return !ReservedLdhLabel.isReservedLdhLabelInternal(label);
        }
    }


    /**
     * A Non-LDH label which does <em>not</em> begin with an underscore ('_'), hyphen ('-') or ends with an hyphen.
     */
    public static final class OtherNonLdhLabel extends NonLdhLabel {

        OtherNonLdhLabel(String label) {
            super(label);
        }

    }


    /**
     * A reserved LDH label (R-LDH label), which have the property that they contain "--" in the third and fourth characters.
     */
    public static class ReservedLdhLabel extends LdhLabel {

        protected ReservedLdhLabel(String label) {
            super(label);
            assert isReservedLdhLabelInternal(label);
        }

        public static boolean isReservedLdhLabel(String label) {
            if (!isLdhLabel(label)) {
                return false;
            }
            return isReservedLdhLabelInternal(label);
        }

        static boolean isReservedLdhLabelInternal(String label) {
            return label.length() >= 4
                    && label.charAt(2) == '-'
                    && label.charAt(3) == '-';
        }
    }


    /**
     * A DNS label which begins with an underscore ('_').
     */
    public static final class UnderscoreLabel extends NonLdhLabel {

        UnderscoreLabel(String label) {
            super(label);
        }

        static boolean isUnderscoreLabelInternal(String label) {
            return label.charAt(0) == '_';
        }
    }


    /**
     * A label that begins with "xn--" and follows the LDH rule.
     */
    public static abstract class XnLabel extends ReservedLdhLabel {

        protected XnLabel(String label) {
            super(label);
        }

        protected static LdhLabel fromInternal(String label) {
            assert isIdnAcePrefixed(label);

            String uLabel = DnsIdna.toUnicode(label);
            if (label.equals(uLabel)) {
                // No Punycode conversation to Unicode was performed, this is a fake A-label!
                return new FakeALabel(label);
            } else {
                return new ALabel(label);
            }
        }

        static boolean isXnLabelInternal(String label) {
            // Note that we already ensure the minimum label length here, since reserved LDH
            // labels must start with "xn--".
            return label.substring(0, 2).toLowerCase(Locale.US).equals("xn");
        }
    }


    /**
     * A DNS label which contains more than just letters, digits and a hyphen.
     */
    public static abstract class NonLdhLabel extends DnsLabel {

        protected NonLdhLabel(String label) {
            super(label);
        }

        protected static DnsLabel fromInternal(String label) {
            if (UnderscoreLabel.isUnderscoreLabelInternal(label)) {
                return new UnderscoreLabel(label);
            }

            if (LeadingOrTrailingHyphenLabel.isLeadingOrTrailingHypenLabelInternal(label)) {
                return new LeadingOrTrailingHyphenLabel(label);
            }

            return new OtherNonLdhLabel(label);
        }

    }


    /**
     * A LDH (<b>L</b>etters, <b>D</b>igits, <b>H</b>yphen) label, which is the
     * classical label form.
     * <p>
     * Note that it is a common misconception that LDH labels can not start with a
     * digit. The origin of this misconception is likely that
     * <a href="https://datatracker.ietf.org/doc/html/rfc1034#section-3.5">RFC 1034
     * § 3.5</a> specified
     * </p>
     * <blockquote>
     * They [i.e, DNS labels] must start with a letter, end with a letter or digit,
     * and have as interior characters only letters, digits, and hyphen.
     * </blockquote>.
     * However, this was relaxed in
     * <a href="https://datatracker.ietf.org/doc/html/rfc1123#page-13">RFC 1123 §
     * 2.1</a>
     * <blockquote>
     * One aspect of host name syntax is hereby changed: the restriction on the first
     * character is relaxed to allow either a letter or a digit.
     * </blockquote>
     * and later summarized in
     * <a href="https://datatracker.ietf.org/doc/html/rfc3696#section-2">RFC 3696 §
     * 2</a>:
     * <blockquote>
     * If the hyphen is used, it is not permitted to appear at either the beginning
     * or end of a label.
     * </blockquote>
     * Furthermore
     * <a href="https://datatracker.ietf.org/doc/html/rfc5890#section-2.3.1">RFC
     * 5890 § 2.3.1</a> only mentions the requirement that hyphen must not be the
     * first or last character of a LDH label.
     *
     * @see <a href="https://tools.ietf.org/html/rfc5890#section-2.3.1">RFC 5890 §
     * 2.3.1. LDH Label</a>
     */
    public static abstract class LdhLabel extends DnsLabel {

        protected LdhLabel(String label) {
            super(label);
        }

        public static boolean isLdhLabel(String label) {
            if (label.isEmpty()) {
                return false;
            }

            if (LeadingOrTrailingHyphenLabel.isLeadingOrTrailingHypenLabelInternal(label)) {
                return false;
            }

            return consistsOnlyOfLettersDigitsAndHypen(label);
        }

        protected static LdhLabel fromInternal(String label) {
            assert isLdhLabel(label);

            if (ReservedLdhLabel.isReservedLdhLabel(label)) {
                // Label starts with '??--'. Now let us see if it is a XN-Label, starting with 'xn--', but be aware that the
                // 'xn' part is case insensitive. The XnLabel.isXnLabelInternal(String) method takes care of this.
                if (XnLabel.isXnLabelInternal(label)) {
                    return XnLabel.fromInternal(label);
                } else {
                    return new ReservedLdhLabel(label);
                }
            }
            return new NonReservedLdhLabel(label);
        }
    }

}
