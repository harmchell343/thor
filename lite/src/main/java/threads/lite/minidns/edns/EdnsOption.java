/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns.edns;

import androidx.annotation.NonNull;

import java.io.DataOutputStream;
import java.io.IOException;

import threads.lite.minidns.edns.Edns.OptionCode;

public abstract class EdnsOption {

    public final int optionCode;
    public final int optionLength;

    protected final byte[] optionData;
    private String toStringCache;
    private String terminalOutputCache;

    protected EdnsOption(int optionCode, byte[] optionData) {
        this.optionCode = optionCode;
        this.optionLength = optionData.length;
        this.optionData = optionData;
    }

    protected EdnsOption(byte[] optionData) {
        this.optionCode = getOptionCode().asInt;
        this.optionLength = optionData.length;
        this.optionData = optionData;
    }

    public static EdnsOption parse(int intOptionCode, byte[] optionData) {
        OptionCode optionCode = OptionCode.from(intOptionCode);
        EdnsOption res;
        if (optionCode == OptionCode.NSID) {
            res = new Nsid(optionData);
        } else {
            res = new UnknownEdnsOption(intOptionCode, optionData);
        }
        return res;
    }

    public final void writeToDos(DataOutputStream dos) throws IOException {
        dos.writeShort(optionCode);
        dos.writeShort(optionLength);
        dos.write(optionData);
    }

    public abstract OptionCode getOptionCode();

    @NonNull
    @Override
    public final String toString() {
        if (toStringCache == null) {
            toStringCache = toStringInternal().toString();
        }
        return toStringCache;
    }

    protected abstract CharSequence toStringInternal();

    public final String asTerminalOutput() {
        if (terminalOutputCache == null) {
            terminalOutputCache = asTerminalOutputInternal().toString();
        }
        return terminalOutputCache;
    }

    protected abstract CharSequence asTerminalOutputInternal();

}
