package threads.lite.swarmstore;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import threads.lite.cid.Multiaddr;

@Dao
public interface SwarmDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMultiaddr(Multiaddr multiaddr);

    @Query("SELECT * FROM Multiaddr")
    List<Multiaddr> getMultiaddrs();

    @Delete
    void removeMultiaddr(Multiaddr multiaddr);
}
