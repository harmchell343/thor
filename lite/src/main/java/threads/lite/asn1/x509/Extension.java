package threads.lite.asn1.x509;

import threads.lite.asn1.ASN1Boolean;
import threads.lite.asn1.ASN1EncodableVector;
import threads.lite.asn1.ASN1Object;
import threads.lite.asn1.ASN1ObjectIdentifier;
import threads.lite.asn1.ASN1OctetString;
import threads.lite.asn1.ASN1Primitive;
import threads.lite.asn1.ASN1Sequence;
import threads.lite.asn1.DEROctetString;
import threads.lite.asn1.DERSequence;
import threads.lite.asn1.util.Arrays;

/**
 * an object for the elements in the X.509 V3 extension block.
 */
public class Extension extends ASN1Object {
    /**
     * Subject Directory Attributes
     */
    public static final ASN1ObjectIdentifier subjectDirectoryAttributes = new ASN1ObjectIdentifier("2.5.29.9").intern();

    /**
     * Subject Alternative Name
     */
    public static final ASN1ObjectIdentifier subjectAlternativeName = new ASN1ObjectIdentifier("2.5.29.17").intern();

    /**
     * Issuer Alternative Name
     */
    public static final ASN1ObjectIdentifier issuerAlternativeName = new ASN1ObjectIdentifier("2.5.29.18").intern();

    /**
     * Certificate Issuer
     */
    public static final ASN1ObjectIdentifier certificateIssuer = new ASN1ObjectIdentifier("2.5.29.29").intern();

    private final ASN1ObjectIdentifier extnId;
    private final boolean critical;
    private final ASN1OctetString value;

    /**
     * Constructor using a byte[] for the value.
     *
     * @param extnId   the OID associated with this extension.
     * @param critical true if the extension is critical, false otherwise.
     * @param value    the extension's value as a byte[] to be wrapped in an OCTET STRING.
     */
    public Extension(ASN1ObjectIdentifier extnId, boolean critical, byte[] value) {
        this(extnId, critical, new DEROctetString(Arrays.clone(value)));
    }

    /**
     * Constructor using an OCTET STRING for the value.
     *
     * @param extnId   the OID associated with this extension.
     * @param critical true if the extension is critical, false otherwise.
     * @param value    the extension's value wrapped in an OCTET STRING.
     */
    public Extension(ASN1ObjectIdentifier extnId, boolean critical, ASN1OctetString value) {
        this.extnId = extnId;
        this.critical = critical;
        this.value = value;
    }

    private Extension(ASN1Sequence seq) {
        if (seq.size() == 2) {
            this.extnId = ASN1ObjectIdentifier.getInstance(seq.getObjectAt(0));
            this.critical = false;
            this.value = ASN1OctetString.getInstance(seq.getObjectAt(1));
        } else if (seq.size() == 3) {
            this.extnId = ASN1ObjectIdentifier.getInstance(seq.getObjectAt(0));
            this.critical = ASN1Boolean.getInstance(seq.getObjectAt(1)).isTrue();
            this.value = ASN1OctetString.getInstance(seq.getObjectAt(2));
        } else {
            throw new IllegalArgumentException("Bad sequence size: " + seq.size());
        }
    }

    public static Extension getInstance(Object obj) {
        if (obj instanceof Extension) {
            return (Extension) obj;
        } else if (obj != null) {
            return new Extension(ASN1Sequence.getInstance(obj));
        }

        return null;
    }

    public ASN1ObjectIdentifier getExtnId() {
        return extnId;
    }

    public boolean isCritical() {
        return critical;
    }

    public ASN1OctetString getExtnValue() {
        return value;
    }

    public int hashCode() {
        if (this.isCritical()) {
            return this.getExtnValue().hashCode() ^ this.getExtnId().hashCode();
        }

        return ~(this.getExtnValue().hashCode() ^ this.getExtnId().hashCode());
    }

    public boolean equals(
            Object o) {
        if (!(o instanceof Extension)) {
            return false;
        }

        Extension other = (Extension) o;

        return other.getExtnId().equals(this.getExtnId())
                && other.getExtnValue().equals(this.getExtnValue())
                && (other.isCritical() == this.isCritical());
    }

    public ASN1Primitive toASN1Primitive() {
        ASN1EncodableVector v = new ASN1EncodableVector(3);

        v.add(extnId);

        if (critical) {
            v.add(ASN1Boolean.getInstance(true));
        }

        v.add(value);

        return new DERSequence(v);
    }
}
