package threads.lite.asn1.x500;

import androidx.annotation.NonNull;

import java.util.Enumeration;

import threads.lite.asn1.ASN1Choice;
import threads.lite.asn1.ASN1Encodable;
import threads.lite.asn1.ASN1Object;
import threads.lite.asn1.ASN1Primitive;
import threads.lite.asn1.ASN1Sequence;
import threads.lite.asn1.DERSequence;
import threads.lite.asn1.x500.style.BCStyle;

/**
 * The X.500 Name object.
 * <pre>
 *     Name ::= CHOICE {
 *                       RDNSequence }
 *
 *     RDNSequence ::= SEQUENCE OF RelativeDistinguishedName
 *
 *     RelativeDistinguishedName ::= SET SIZE (1..MAX) OF AttributeTypeAndValue
 *
 *     AttributeTypeAndValue ::= SEQUENCE {
 *                                   type  OBJECT IDENTIFIER,
 *                                   value ANY }
 * </pre>
 */
public class X500Name extends ASN1Object implements ASN1Choice {

    private final RDN[] rdns;
    private final DERSequence rdnSeq;
    private final X500NameStyle style;
    private boolean isHashCodeCalculated;
    private int hashCodeValue;


    /**
     * Constructor from ASN1Sequence
     * <p>
     * the principal will be a list of constructed sets, each containing an (OID, String) pair.
     */
    private X500Name(ASN1Sequence seq) {
        this(BCStyle.getInstance(), seq);
    }

    private X500Name(X500NameStyle style, ASN1Sequence seq) {
        this.style = style;
        this.rdns = new RDN[seq.size()];

        boolean inPlace = true;

        int index = 0;
        for (Enumeration<ASN1Encodable> e = seq.getObjects(); e.hasMoreElements(); ) {
            ASN1Encodable element = e.nextElement();
            RDN rdn = RDN.getInstance(element);
            inPlace &= (rdn == element);
            rdns[index++] = rdn;
        }

        if (inPlace) {
            this.rdnSeq = DERSequence.convert(seq);
        } else {
            this.rdnSeq = new DERSequence(this.rdns);
        }
    }

    public X500Name(X500NameStyle style, RDN[] rDNs) {
        this.style = style;
        this.rdns = rDNs.clone();
        this.rdnSeq = new DERSequence(this.rdns);
    }

    public X500Name(String dirName) {
        this(BCStyle.getInstance(), dirName);
    }

    public X500Name(X500NameStyle style, String dirName) {
        this(style, style.fromString(dirName));

    }

    public static X500Name getInstance(
            Object obj) {
        if (obj instanceof X500Name) {
            return (X500Name) obj;
        } else if (obj != null) {
            return new X500Name(ASN1Sequence.getInstance(obj));
        }

        return null;
    }


    /**
     * return an array of RDNs in structure order.
     *
     * @return an array of RDN objects.
     */
    public RDN[] getRDNs() {
        return rdns.clone();
    }

    public ASN1Primitive toASN1Primitive() {
        return rdnSeq;
    }

    public int hashCode() {
        if (isHashCodeCalculated) {
            return hashCodeValue;
        }

        isHashCodeCalculated = true;

        hashCodeValue = style.calculateHashCode(this);

        return hashCodeValue;
    }

    /**
     * test for equality - note: case is ignored.
     */
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof X500Name || obj instanceof ASN1Sequence)) {
            return false;
        }

        ASN1Primitive derO = ((ASN1Encodable) obj).toASN1Primitive();

        if (this.toASN1Primitive().equals(derO)) {
            return true;
        }

        try {
            return style.areEqual(this, new X500Name(ASN1Sequence.getInstance(((ASN1Encodable) obj).toASN1Primitive())));
        } catch (Exception e) {
            return false;
        }
    }

    @NonNull
    public String toString() {
        return style.toString(this);
    }
}
