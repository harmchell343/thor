package threads.thor.core.tabs;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import java.io.ByteArrayOutputStream;
import java.util.Objects;

@androidx.room.Entity
public class Tab {
    @NonNull
    @ColumnInfo(name = "uri")
    private final String uri;
    @NonNull
    @ColumnInfo(name = "title")
    private final String title;
    @Nullable
    @ColumnInfo(name = "image", typeAffinity = ColumnInfo.BLOB)
    private final byte[] image;

    @PrimaryKey(autoGenerate = true)
    private long idx;

    public Tab(@NonNull String title, @NonNull String uri,
               @Nullable byte[] image) {
        this.uri = uri;
        this.title = title;
        this.image = image;
    }

    @NonNull
    public static Tab createTab(@NonNull String title, @NonNull String uri,
                                @Nullable byte[] image) {
        return new Tab(title, uri, image);
    }

    @Nullable
    static byte[] getImageBytes(@Nullable Bitmap image) {
        try {
            if (image == null) {
                return null;
            }
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 10, stream);
            return stream.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @Nullable
    public byte[] getImage() {
        return image;
    }


    @NonNull
    @Override
    public String toString() {
        return "Tab{" +
                "uri='" + uri + '\'' +
                ", title='" + title + '\'' +
                ", idx=" + idx +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tab tab = (Tab) o;
        return idx == tab.idx;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idx);
    }

    @Nullable
    public Bitmap getBitmapImage() {
        if (image != null) {
            return BitmapFactory.decodeByteArray(image, 0, image.length);
        }
        return null;
    }

    @NonNull
    public String getUri() {
        return uri;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public long getIdx() {
        return idx;
    }

    public void setIdx(long idx) {
        this.idx = idx;
    }

}
