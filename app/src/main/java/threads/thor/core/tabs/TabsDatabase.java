package threads.thor.core.tabs;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {Tab.class}, version = 1, exportSchema = false)
public abstract class TabsDatabase extends RoomDatabase {

    public abstract TabDao tabDao();

}
