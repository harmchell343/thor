package threads.thor.core.tabs;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.List;

import threads.thor.core.events.EVENTS;

public class TABS {
    private static volatile TABS INSTANCE = null;

    private final TabsDatabase tabsDatabase;

    private TABS(TabsDatabase tabsDatabase) {
        this.tabsDatabase = tabsDatabase;
    }

    public static TABS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (EVENTS.class) {
                if (INSTANCE == null) {
                    TabsDatabase tabsDatabase =
                            Room.inMemoryDatabaseBuilder(context,
                                            TabsDatabase.class).
                                    allowMainThreadQueries().build();
                    INSTANCE = new TABS(tabsDatabase);
                }
            }
        }
        return INSTANCE;
    }


    public void createTab(@NonNull String title, @NonNull String uri, @Nullable Bitmap image) {
        Tab tab = Tab.createTab(title, uri, Tab.getImageBytes(image));
        tabsDatabase.tabDao().insertTab(tab);
    }

    @Nullable
    public Tab getTab(long idx) {
        return tabsDatabase.tabDao().getTab(idx);
    }

    /**
     * @noinspection unused
     */
    @NonNull
    public List<Tab> getTabs() {
        return tabsDatabase.tabDao().getTabs();
    }

    @NonNull
    public TabsDatabase getTabsDatabase() {
        return tabsDatabase;
    }

    public void updateTab(long tabIdx, @NonNull String title,
                          @NonNull String uri, @Nullable Bitmap image) {
        tabsDatabase.tabDao().updateTab(tabIdx, title, uri, Tab.getImageBytes(image));
    }

    public void clear() {
        tabsDatabase.clearAllTables();
    }

    public void removeTab(Tab tab) {
        tabsDatabase.tabDao().deleteTab(tab);
    }

    public boolean hasTabs() {
        return tabsDatabase.tabDao().hasTabs();
    }
}
