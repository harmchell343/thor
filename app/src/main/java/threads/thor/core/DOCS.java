package threads.thor.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebResourceResponse;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;
import threads.thor.InitApplication;
import threads.thor.LogUtils;
import threads.thor.core.books.BOOKS;
import threads.thor.core.locals.LOCALS;
import threads.thor.services.MimeTypeService;

public class DOCS {

    public static final int CLICK_OFFSET = 500;
    public static final String SERVICE = "_p2p._udp";
    public static final String MAGNET_SCHEME = "magnet";
    public static final String IPFS_SCHEME = "ipfs";
    public static final String IPNS_SCHEME = "ipns";
    public static final String HTTPS_SCHEME = "https";
    public static final String HTTP_SCHEME = "http";
    public static final String TAB = "tab";
    private static final String TAG = DOCS.class.getSimpleName();
    private static final int QR_CODE_SIZE = 250;
    @NonNull
    private final static ConcurrentHashMap<PeerId, Root> resolves = new ConcurrentHashMap<>();
    private static volatile DOCS INSTANCE = null;
    private final IPFS ipfs;
    private final BOOKS books;
    private final LOCALS locals;

    private DOCS(@NonNull Context context) throws Exception {
        long start = System.currentTimeMillis();
        locals = LOCALS.getInstance();
        ipfs = IPFS.getInstance(context);
        books = BOOKS.getInstance(context);

        LogUtils.info(InitApplication.TIME_TAG, "DOCS finish [" +
                (System.currentTimeMillis() - start) + "]...");
    }

    public static DOCS getInstance(@NonNull Context context) throws Exception {

        if (INSTANCE == null) {
            synchronized (DOCS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new DOCS(context);
                }
            }
        }
        return INSTANCE;
    }


    @NonNull
    public static String getFileName(@NonNull Uri uri) {

        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            return paths.get(paths.size() - 1);
        } else {
            return "" + uri.getHost();
        }

    }

    @NonNull
    public static Uri redirectHttp(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), HTTP_SCHEME)) {
                String host = uri.getHost();
                Objects.requireNonNull(host);
                if (Objects.equals(host, "localhost") || Objects.equals(host, "127.0.0.1")) {
                    List<String> paths = uri.getPathSegments();
                    if (paths.size() >= 2) {
                        String protocol = paths.get(0);
                        String authority = paths.get(1);
                        List<String> subPaths = new ArrayList<>(paths);
                        subPaths.remove(protocol);
                        subPaths.remove(authority);
                        Cid.decode(authority); // make sure authority is valid

                        if (Objects.equals(protocol, IPFS_SCHEME)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(IPFS_SCHEME)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        } else if (Objects.equals(protocol, IPNS_SCHEME)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(IPNS_SCHEME)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        }
                    }

                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return uri;
    }

    @NonNull
    public static Uri redirectHttps(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), HTTPS_SCHEME)) {
                List<String> paths = uri.getPathSegments();
                if (paths.size() >= 2) {
                    String protocol = paths.get(0);
                    if (Objects.equals(protocol, IPFS_SCHEME) ||
                            Objects.equals(protocol, IPNS_SCHEME)) {
                        String authority = paths.get(1);
                        List<String> subPaths = new ArrayList<>(paths);
                        subPaths.remove(protocol);
                        subPaths.remove(authority);
                        Cid.decode(authority); // make sure authority is valid
                        if (Objects.equals(protocol, IPFS_SCHEME)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(IPFS_SCHEME)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        } else if (Objects.equals(protocol, IPNS_SCHEME)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(IPNS_SCHEME)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        }
                    }

                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return uri;
    }

    public static Bitmap getQRCode(@NonNull String contents) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(contents,
                    BarcodeFormat.QR_CODE, QR_CODE_SIZE, QR_CODE_SIZE);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            return barcodeEncoder.createBitmap(bitMatrix);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public static WebResourceResponse createEmptyResource() {
        return new WebResourceResponse(MimeTypeService.PLAIN_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream("".getBytes()));
    }

    public static WebResourceResponse createErrorMessage(@NonNull Throwable throwable) {
        LogUtils.error(TAG, throwable);
        String message = generateErrorHtml(throwable);
        return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(message.getBytes()));
    }

    public static String generateErrorHtml(@NonNull Throwable throwable) {
        String message = throwable.getMessage();
        if (message == null || message.isEmpty()) {
            message = throwable.getClass().getSimpleName();
        }
        return "<html>" + "<head>" + MimeTypeService.META +
                "<title>" + "Error" + "</title>" +
                "</head>\n" + MimeTypeService.STYLE +
                "<body><div <div>" + message + "</div></body></html>";
    }

    @NonNull
    private String getMimeType(@NonNull Session session, @NonNull Cid cid,
                               @NonNull Cancellable closeable) throws Exception {

        if (ipfs.isDir(session, cid, closeable)) {
            return MimeTypeService.DIR_MIME_TYPE;
        }

        return MimeTypeService.OCTET_MIME_TYPE;
    }

    @Nullable
    public String getHost(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), IPNS_SCHEME)) {
                return uri.getHost();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    @Nullable
    private Root resolveLocal(Session session, PeerId peerId) {

        try {
            Multiaddr multiaddr = locals.getLocalAddress(peerId);
            if (multiaddr != null) {
                Connection connection = session.connect(multiaddr, ipfs.getConnectionParameters());
                IpnsEntity ipnsEntity =
                        ipfs.pull(connection).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                if (Objects.equals(ipnsEntity.getPeerId(), peerId)) {
                    ipfs.getPageStore().storePage(ipnsEntity);
                    Cid resolvedCid = ipfs.decodeIpnsData(ipnsEntity);
                    Root root = createRoot(session, resolvedCid, ipnsEntity.getName());
                    addResolves(ipnsEntity.getPeerId(), root);
                    return root;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return null;
    }

    @NonNull
    private Root createRoot(@NonNull Session session, @NonNull Cid cid, @Nullable String name) {
        if (name != null) {
            FindProviders findProviders = (FindProviders) session.getFindProviders();
            findProviders.deactivate();
        }
        return new Root(cid, name);

    }

    @Nullable
    private Root resolveSwarm(Session session, PeerId peerId) {

        try {
            Connection connection = session.getConnection(peerId);
            if (connection != null) {
                IpnsEntity ipnsEntity =
                        ipfs.pull(connection).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                if (Objects.equals(ipnsEntity.getPeerId(), peerId)) {
                    ipfs.getPageStore().storePage(ipnsEntity);
                    Cid resolvedCid = ipfs.decodeIpnsData(ipnsEntity);
                    Root root = createRoot(session, resolvedCid, ipnsEntity.getName());
                    addResolves(ipnsEntity.getPeerId(), root);
                    return root;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return null;
    }

    @NonNull
    public Root resolveName(@NonNull Session session, @NonNull Uri uri, @NonNull PeerId peerId,
                            @NonNull Cancellable closeable) throws Exception {

        Root resolved = resolves.get(peerId);
        if (resolved != null) {
            return resolved;
        }

        resolved = resolveSwarm(session, peerId);
        if (resolved != null) {
            return resolved;
        }

        resolved = resolveLocal(session, peerId);
        if (resolved != null) {
            return resolved;
        }

        long sequence = 0L;
        Cid cid = null;
        String name = null;
        long timeout = IPFS.MAX_TIMEOUT;
        boolean shortcut = false;

        IpnsEntity page = ipfs.getPageStore().getPage(peerId);
        if (page != null) {
            sequence = page.getSequence();
            name = page.getName();
            cid = IpnsEntity.decodeIpnsData(page.getValue());
            if (page.getEolDate().after(new Date(System.currentTimeMillis()))) {
                // page is still valid, therefore timeout just 30 sec
                timeout = 30;
            }

            // now we know that the previous page was get directly (so IPFS Lite server)
            if (Objects.equals(page.getProtocol(), IPFS.LITE_PULL_PROTOCOL)) {
                shortcut = true;
            }
        }

        IpnsEntity ipnsEntity = null;
        if (shortcut) {
            try {
                ipnsEntity = ipfs.resolveName(session, peerId,
                                new TimeoutCancellable(closeable, timeout))
                        .get(timeout, TimeUnit.SECONDS);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage());

                // todo throw detailed exception why it has failed
                // (1) no connection to server
                // (2) different ip - versions (ipv4 versus ipv6)
                // when old page is shown give message and
                // it should automatically updated when resolve names succeeds
            }
        } else {
            ipnsEntity = ipfs.resolve(session, peerId, sequence,
                    new TimeoutCancellable(closeable, timeout));
        }
        if (ipnsEntity == null) {
            if (cid != null) {
                Root root = createRoot(session, cid, name);
                resolves.put(peerId, root);
                return root;
            }
            throw new ResolveNameException(uri.toString());
        }

        if (Objects.equals(ipnsEntity.getPeerId(), peerId)) {
            ipfs.getPageStore().storePage(ipnsEntity);
            Cid resolvedCid = ipfs.decodeIpnsData(ipnsEntity);
            Root root = createRoot(session, resolvedCid, ipnsEntity.getName());
            addResolves(ipnsEntity.getPeerId(), root);
            return root;
        }
        throw new ResolveNameException(uri.toString());
    }

    public void addResolves(@NonNull PeerId peerId,
                            @NonNull Root root) {
        resolves.put(peerId, root);
    }

    public WebResourceResponse createRedirectMessage(@NonNull Uri uri) {
        return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(("<!DOCTYPE HTML>\n" +
                "<html lang=\"en-US\">\n" +
                "    <head>\n" + MimeTypeService.META +
                "        <meta http-equiv=\"refresh\" content=\"0; url=" + uri + "\">\n" +
                "        <title>Page Redirection</title>\n" +
                "    </head>\n" + MimeTypeService.STYLE +
                "    <body>\n" +
                "        Automatically redirected to the <a style=\"word-wrap: break-word;\" href='" + uri + "'>" + uri + "</a> location\n" +
                "</html>").getBytes()));
    }

    public String generateDirectoryHtml(@NonNull Root root,
                                        @NonNull Uri uri,
                                        @NonNull List<String> paths,
                                        @Nullable List<Link> links) {
        String title = root.getName();

        if (paths.isEmpty()) {
            if (title == null || title.isBlank()) {
                title = uri.getHost();
            }
        } else {
            title = paths.get(paths.size() - 1); // get last
        }


        StringBuilder answer = new StringBuilder("<html>" + "<head>" + MimeTypeService.META +
                "<title>" + title + "</title>");

        answer.append("</head>");
        answer.append(MimeTypeService.STYLE);
        answer.append("<body>");

        if (links != null) {
            if (!links.isEmpty()) {
                answer.append("<form><table  width=\"100%\" style=\"border-spacing: 8px;\">");

                // folder up
                if (!paths.isEmpty()) {
                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme(uri.getScheme()).authority(uri.getAuthority());
                    for (int i = 0; i < paths.size() - 1; i++) {
                        String path = paths.get(i);
                        builder.appendPath(path);
                    }

                    Uri linkUri = builder.build();
                    answer.append("<tr>");
                    answer.append("<td align=\"center\">");
                    answer.append("<a href=\"");
                    answer.append(linkUri.toString());
                    answer.append("\">");
                    answer.append(MimeTypeService.FOLDER_UP);
                    answer.append("</a>");

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">");
                    answer.append("..");
                    answer.append("</td>");
                    answer.append("<td/>");
                    answer.append("<td/>");
                    answer.append("</td>");
                    answer.append("</tr>");
                }


                for (Link link : links) {

                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme(uri.getScheme()).authority(uri.getAuthority());
                    for (String path : paths) {
                        builder.appendPath(path);
                    }
                    builder.appendPath(link.getName());
                    builder.appendQueryParameter("download", "0");
                    Uri linkUri = builder.build();
                    answer.append("<tr>");

                    answer.append("<td>");
                    answer.append(MimeTypeService.getSvgResource(link.getName()));
                    answer.append("</td>");

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">");
                    answer.append("<a href=\"");
                    answer.append(linkUri.toString());
                    answer.append("\">");
                    answer.append(link.getName());
                    answer.append("</a>");
                    answer.append("</td>");

                    answer.append("<td>");
                    answer.append(getFileSize(link.getSize()));
                    answer.append("</td>");

                    answer.append("<td align=\"center\">");
                    String text = "<button style=\"float:none!important;display:inline;\" " +
                            "name=\"download\" value=\"1\" formenctype=\"text/plain\" " +
                            "formmethod=\"get\" type=\"submit\" formaction=\"" +
                            linkUri + "\">" + MimeTypeService.getSvgDownload() + "</button>";
                    answer.append(text);
                    answer.append("</td>");
                    answer.append("</tr>");
                }
                answer.append("</table></form>");
            }

        }

        ZonedDateTime zoned = ZonedDateTime.now();
        DateTimeFormatter pattern = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);

        answer.append("</body><div class=\"footer\">")
                .append("<p>")
                .append(zoned.format(pattern))
                .append("</p>")
                .append("</div></html>");


        return answer.toString();
    }

    private String getFileSize(long size) {

        String fileSize;

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize.concat(" B");
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize.concat(" KB");
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize.concat(" MB");
        }
    }

    @NonNull
    public WebResourceResponse getResponse(@NonNull Session session, @NonNull Root root,
                                           @NonNull Uri uri, @NonNull Cancellable closeable)
            throws Exception {
        List<String> paths = uri.getPathSegments();
        if (paths.isEmpty()) {
            if (ipfs.isDir(session, root.getCid(), closeable)) {
                List<Link> links = ipfs.links(session, root.getCid(), false, closeable);
                String answer = generateDirectoryHtml(root, uri, paths, links);
                return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE,
                        StandardCharsets.UTF_8.name(), new ByteArrayInputStream(answer.getBytes()));
            } else {
                String mimeType = MimeTypeService.OCTET_MIME_TYPE;
                return getContentResponse(session, root.getCid(), mimeType, closeable);
            }


        } else {
            Cid cid = ipfs.resolveCid(session, root.getCid(), paths, closeable);
            if (ipfs.isDir(session, cid, closeable)) {
                List<Link> links = ipfs.links(session, cid, false, closeable);
                String answer = generateDirectoryHtml(root, uri, paths, links);
                return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE,
                        StandardCharsets.UTF_8.name(), new ByteArrayInputStream(answer.getBytes()));

            } else {
                String mimeType = getMimeType(session, uri, cid, closeable);
                return getContentResponse(session, cid, mimeType, closeable);
            }
        }
    }

    @NonNull
    private WebResourceResponse getContentResponse(@NonNull Session session,
                                                   @NonNull Cid cid, @NonNull String mimeType,
                                                   @NonNull Cancellable closeable)
            throws Exception {

        try (InputStream in = ipfs.getInputStream(session, cid, closeable)) {

            Map<String, String> responseHeaders = new HashMap<>();

            return new WebResourceResponse(mimeType, StandardCharsets.UTF_8.name(), 200,
                    "OK", responseHeaders, new BufferedInputStream(in));
        }


    }

    @NonNull
    public String getMimeType(@NonNull Session session, @NonNull Uri uri,
                              @NonNull Cid cid, @NonNull Cancellable closeable)
            throws Exception {

        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            String name = paths.get(paths.size() - 1);
            String mimeType = MimeTypeService.getMimeType(name);
            if (!mimeType.equals(MimeTypeService.OCTET_MIME_TYPE)) {
                return mimeType;
            } else {
                return getMimeType(session, cid, closeable);
            }
        } else {
            return getMimeType(session, cid, closeable);
        }

    }

    @Nullable
    public Content getContent(@NonNull Session session, @NonNull Uri uri,
                              @NonNull Cancellable closeable) throws Exception {

        String host = uri.getHost();
        Objects.requireNonNull(host);

        Root root = getRoot(session, uri, closeable);

        List<String> paths = uri.getPathSegments();

        return new Content(root, ipfs.resolveCid(session, root.getCid(), paths, closeable));
    }


    @NonNull
    private Root resolveHost(@NonNull Session session, @NonNull Uri uri,
                             @NonNull String host, @NonNull Cancellable closeable)
            throws Exception {
        String link = ipfs.resolveDnsLink(host);
        if (link.isEmpty()) {
            // could not resolved, maybe no NETWORK
            String dnsLink = books.getDnsLink(uri.toString());
            if (dnsLink == null) {
                throw new DOCS.ResolveNameException(uri.toString());
            } else {
                return resolveDnsLink(session, uri, dnsLink, closeable);
            }
        } else {
            if (link.startsWith(IPFS.IPFS_PATH)) {
                // try to store value
                books.storeDnsLink(uri.toString(), link);
                return resolveDnsLink(session, uri, link, closeable);
            } else if (link.startsWith(IPFS.IPNS_PATH)) {
                return resolveHost(session, uri,
                        link.replaceFirst(IPFS.IPNS_PATH, ""),
                        closeable);

            } else {
                throw new DOCS.ResolveNameException(uri.toString());
            }
        }
    }

    @NonNull
    private Root resolveUri(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable closeable)
            throws Exception {
        String host = uri.getHost();
        Objects.requireNonNull(host);

        if (!Objects.equals(uri.getScheme(), IPNS_SCHEME)) {
            throw new RuntimeException();
        }
        try {
            PeerId peerId = ipfs.decodePeerId(host);
            return resolveName(session, uri, peerId, closeable);
        } catch (Throwable ignore) {
            return resolveHost(session, uri, host, closeable);
        }
    }

    @NonNull
    private Root getRoot(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable cancellable)
            throws Exception {
        String host = uri.getHost();
        Objects.requireNonNull(host);

        Root root;
        if (Objects.equals(uri.getScheme(), IPNS_SCHEME)) {
            root = resolveUri(session, uri, cancellable);
        } else {
            try {
                root = createRoot(session, Cid.decode(host), null);
            } catch (Throwable throwable) {
                throw new InvalidNameException(uri.toString());
            }
        }

        if (!root.getCid().isSupported()) {
            throw new ResolveNameException("Encoding type '" +
                    root.getCid().getPrefix().getType().name() +
                    "' is not supported." +
                    "Currently only 'sha2_256' encoded CID's are supported.");
        }

        return root;
    }

    @NonNull
    public WebResourceResponse getResponse(@NonNull Session session, @NonNull Uri uri,
                                           @NonNull Cancellable cancellable) throws Exception {
        Root root = getRoot(session, uri, cancellable);
        return getResponse(session, root, uri, cancellable);
    }

    @NonNull
    private Root resolveDnsLink(@NonNull Session session, @NonNull Uri uri, @NonNull String link,
                                @NonNull Cancellable closeable)
            throws Exception {

        List<String> paths = uri.getPathSegments();
        if (link.startsWith(IPFS.IPFS_PATH)) {
            return createRoot(session,
                    Cid.decode(link.replaceFirst(IPFS.IPFS_PATH, "")), null);
        } else if (link.startsWith(IPFS.IPNS_PATH)) {
            String pid = link.replaceFirst(IPFS.IPNS_PATH, "");
            try {
                PeerId peerId = ipfs.decodePeerId(pid);
                // is is assume like /ipns/<dns_link> = > therefore <dns_link> is url
                return resolveName(session, uri, peerId, closeable);
            } catch (Throwable ignore) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(IPNS_SCHEME).authority(pid);
                for (String path : paths) {
                    builder.appendPath(path);
                }
                return resolveUri(session, builder.build(), closeable);
            }
        } else {
            // is is assume that links is  <dns_link> is url

            Uri dnsUri = Uri.parse(link);
            if (dnsUri != null) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(IPNS_SCHEME)
                        .authority(dnsUri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                return resolveUri(session, builder.build(), closeable);
            }
        }
        throw new ResolveNameException(uri.toString());
    }

    @NonNull
    public Uri redirectUri(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable closeable)
            throws Exception {


        if (Objects.equals(uri.getScheme(), IPNS_SCHEME) ||
                Objects.equals(uri.getScheme(), IPFS_SCHEME)) {
            List<String> paths = uri.getPathSegments();
            Root root = getRoot(session, uri, closeable);
            return redirect(session, uri, root, paths, closeable);
        }
        return uri;
    }

    @NonNull
    private Uri redirect(@NonNull Session session, @NonNull Uri uri, @NonNull Root root,
                         @NonNull List<String> paths, @NonNull Cancellable closeable)
            throws Exception {


        Cid cid = ipfs.resolveCid(session, root.getCid(), paths, closeable);

        if (ipfs.isDir(session, cid, closeable)) {
            boolean exists = ipfs.hasLink(session, cid, IPFS.INDEX_HTML, closeable);

            if (exists) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(uri.getScheme())
                        .authority(uri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                builder.appendPath(IPFS.INDEX_HTML);
                return builder.build();
            }
        }


        return uri;
    }

    public void cleanupResolver(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), IPNS_SCHEME)) {
                String host = getHost(uri);
                if (host != null) {
                    PeerId peerId = ipfs.decodePeerId(host);
                    resolves.remove(peerId);
                }
            }
        } catch (Throwable ignore) {
            // ignore common failure
        }
    }


    public Set<Multiaddr> getLocals() {
        return locals.getLocals();
    }

    public static class ResolveNameException extends Exception {

        public ResolveNameException(@NonNull String name) {
            super("Resolve name failed for " + name);
        }
    }

    public static class InvalidNameException extends Exception {

        public InvalidNameException(@NonNull String name) {
            super("Invalid name detected for " + name);
        }


    }


    public static class FindProviders implements Function<Cid, Boolean> {
        private boolean deactivate = false;

        public void deactivate() {
            deactivate = true;
        }

        @Override
        public Boolean apply(Cid cid) {
            return !deactivate;
        }
    }

    public static class Content {
        private final Root root;
        private final Cid cid;

        public Content(Root root, Cid cid) {
            this.root = root;
            this.cid = cid;
        }

        public Root getRoot() {
            return root;
        }

        public Cid getCid() {
            return cid;
        }
    }

    public static class Root {
        @NonNull
        private final Cid cid;
        @Nullable
        private final String name;

        public Root(@NonNull Cid cid, @Nullable String name) {
            this.cid = cid;
            this.name = name;
        }

        @NonNull
        public Cid getCid() {
            return cid;
        }

        @Nullable
        public String getName() {
            return name;
        }
    }
}
