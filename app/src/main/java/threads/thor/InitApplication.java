package threads.thor;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.nsd.NsdManager;
import android.os.Build;
import android.provider.DocumentsContract;
import android.util.ArrayMap;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;

import com.google.android.material.color.DynamicColors;

import java.util.Objects;

import threads.lite.IPFS;
import threads.thor.core.DOCS;
import threads.thor.core.events.EVENTS;
import threads.thor.provider.DataProvider;
import threads.thor.services.DiscoveryService;
import threads.thor.services.MimeTypeService;
import threads.thor.utils.AdBlocker;

public class InitApplication extends Application {

    public static final String TIME_TAG = "TIME_TAG";
    public static final String STORAGE_CHANNEL_ID = "STORAGE_CHANNEL_ID";
    public static final String CLEANUP_CHANNEL_ID = "CLEANUP_CHANNEL_ID";

    @NonNull
    private static final ArrayMap<String, String> SEARCH_ENGINES = new ArrayMap<>();
    private static final String TAG = InitApplication.class.getSimpleName();
    private static final String APP_KEY = "AppKey";
    private static final String JAVASCRIPT_KEY = "javascriptKey";
    private static final String REDIRECT_URL_KEY = "redirectUrlKey";
    private static final String REDIRECT_INDEX_KEY = "redirectIndexKey";

    private static final String SEARCH_ENGINE_KEY = "searchEngineKey";
    private NsdManager nsdManager;
    private DiscoveryService discoveryService;

    public static void setJavascriptEnabled(Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(JAVASCRIPT_KEY, auto);
        editor.apply();
    }

    public static boolean isJavascriptEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(JAVASCRIPT_KEY, true);

    }

    public static void setRedirectUrlEnabled(Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(REDIRECT_URL_KEY, auto);
        editor.apply();
    }

    public static boolean isRedirectUrlEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(REDIRECT_URL_KEY, false);
    }

    public static void setRedirectIndexEnabled(@NonNull Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(REDIRECT_INDEX_KEY, auto);
        editor.apply();
    }

    public static boolean isRedirectIndexEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(REDIRECT_INDEX_KEY, true);

    }

    public static String getSearchEngine(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(SEARCH_ENGINE_KEY, context.getString(R.string.duck_duck_go));
    }

    public static void setSearchEngine(@NonNull Context context, @NonNull String searchEngine) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SEARCH_ENGINE_KEY, searchEngine);
        editor.apply();
    }


    @NonNull
    public static Intent getDownloadsIntent() {
        Uri uri = DataProvider.getDownloadsUri();
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType(MimeTypeService.ALL);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uri);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        return intent;
    }


    @SuppressLint("SetJavaScriptEnabled")
    public static void setWebSettings(@NonNull WebView webView, boolean enableJavascript) {


        WebSettings settings = webView.getSettings();
        settings.setUserAgentString("Mozilla/5.0 (Linux; Android " + Build.VERSION.RELEASE + ")");


        settings.setJavaScriptEnabled(enableJavascript);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);

        settings.setSafeBrowsingEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(false);
        settings.setLoadsImagesAutomatically(true);
        settings.setBlockNetworkLoads(false);
        settings.setBlockNetworkImage(false);
        settings.setDomStorageEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setDatabaseEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        settings.setMixedContentMode(WebSettings.MIXED_CONTENT_NEVER_ALLOW);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setMediaPlaybackRequiresUserGesture(true);
        settings.setSupportMultipleWindows(false);
        settings.setGeolocationEnabled(false);
    }

    @NonNull
    public static ArrayMap<String, String> getSearchEngines() {
        return SEARCH_ENGINES;
    }

    @NonNull
    public static Engine getEngine(@NonNull Context context) {
        String searchEngine = InitApplication.getSearchEngine(context);
        String uri = SEARCH_ENGINES.get(searchEngine);
        if (uri == null) {
            return new Engine(context.getString(R.string.duck_duck_go),
                    "https://start.duckduckgo.com/");
        }
        return new Engine(searchEngine, uri);
    }


    private void createStorageChannel(@NonNull Context context) {

        try {
            CharSequence name = context.getString(R.string.storage_channel_name);
            String description = context.getString(R.string.storage_channel_description);
            NotificationChannel mChannel = new NotificationChannel(
                    STORAGE_CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private void createCleanupChannel(@NonNull Context context) {

        try {
            CharSequence name = context.getString(R.string.action_cleanup);
            String description = context.getString(R.string.clear_browser_data);
            NotificationChannel mChannel = new NotificationChannel(
                    CLEANUP_CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterService();
    }

    @Override
    public void onCreate() {
        super.onCreate();


        // adding search engines
        SEARCH_ENGINES.put(getString(R.string.petal_search), "https://www.petalsearch.com/");
        SEARCH_ENGINES.put(getString(R.string.google), "https://www.google.com/");
        SEARCH_ENGINES.put(getString(R.string.duck_duck_go), "https://start.duckduckgo.com/");

        // register service very fast
        registerService();

        // always start with fresh public/private key
        IPFS.setPrivateKey(getApplicationContext(), "");
        IPFS.setPublicKey(getApplicationContext(), "");

        DynamicColors.applyToActivitiesIfAvailable(this);

        long start = System.currentTimeMillis();

        createStorageChannel(getApplicationContext());
        createCleanupChannel(getApplicationContext());

        AdBlocker.init(getApplicationContext());

        EVENTS events = EVENTS.getInstance(getApplicationContext());

        LogUtils.info(TIME_TAG, "InitApplication after add blocker [" +
                (System.currentTimeMillis() - start) + "]...");
        try {
            IPFS.getInstance(getApplicationContext());
        } catch (Throwable throwable) {
            events.fatal(throwable.getClass().getSimpleName() +
                    " " + throwable.getMessage());
            unregisterService();
            LogUtils.error(TAG, throwable);
        }

        LogUtils.info(TIME_TAG, "InitApplication after starting ipfs [" +
                (System.currentTimeMillis() - start) + "]...");

    }

    private void registerService() {
        try {
            nsdManager = (NsdManager) getSystemService(Context.NSD_SERVICE);
            Objects.requireNonNull(nsdManager);

            discoveryService = new DiscoveryService(nsdManager);

            nsdManager.discoverServices(DOCS.SERVICE,
                    NsdManager.PROTOCOL_DNS_SD, discoveryService);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void unregisterService() {
        try {
            if (nsdManager != null) {
                if (discoveryService != null) {
                    nsdManager.stopServiceDiscovery(discoveryService);
                    discoveryService = null;
                }
                nsdManager = null;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    public static class Engine {
        private final String name;
        private final String uri;

        public Engine(String name, String uri) {
            this.name = name;
            this.uri = uri;
        }

        public String getName() {
            return name;
        }

        public String getUri() {
            return uri;
        }
    }
}