package threads.magnet.protocol;

import androidx.annotation.NonNull;

public final class KeepAlive implements Message {

    private static final KeepAlive instance = new KeepAlive();

    private KeepAlive() {
    }

    /**
     * @since 1.0
     */
    public static KeepAlive instance() {
        return instance;
    }

    @NonNull
    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() + "]";
    }

    @Override
    public Integer getMessageId() {
        throw new UnsupportedOperationException();
    }
}
