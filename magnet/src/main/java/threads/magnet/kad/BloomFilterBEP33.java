package threads.magnet.kad;

import static java.lang.Math.log1p;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class BloomFilterBEP33 implements Comparable<BloomFilterBEP33> {

    public final static int m = 256 * 8;
    private final static int k = 2;
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final BitVector filter;

    BloomFilterBEP33() {
        filter = new BitVector(m);
    }

    // the logarithm of the base used for various calculations
    private static double logB() {
        return log1p(-1.0 / m);
    }

    public void insert(InetAddress addr) {
        lock.writeLock().lock();
        try {
            byte[] hash = MessageDigest.getInstance("SHA1").digest(addr.getAddress());

            int index1 = (hash[0] & 0xFF) | (hash[1] & 0xFF) << 8;
            int index2 = (hash[2] & 0xFF) | (hash[3] & 0xFF) << 8;

            // truncate index to m (11 bits required)
            index1 %= m;
            index2 %= m;

            // set bits at index1 and index2
            filter.set(index1);
            filter.set(index2);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        } finally {
            lock.writeLock().unlock();
        }
    }


    public int compareTo(BloomFilterBEP33 o) {
        return size() - o.size();
    }

    private int size() {
        // number of expected 0 bits = m * (1 − 1/m)^(k*size)
        lock.readLock().lock();
        try {
            double c = filter.bitcount();
            double size = log1p(-c / m) / (k * logB());
            return (int) size;
        } finally {
            lock.readLock().unlock();
        }
    }

    public ByteBuffer toBuffer() {
        lock.readLock().lock();
        try {
            return filter.toBuffer();
        } finally {
            lock.readLock().unlock();
        }
    }

}
